
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        String name, message;
        
        BufferedReader dogReader= new BufferedReader (new InputStreamReader(System.in));
        System.out.print("Anna koiralle nimi: ");
        name=dogReader.readLine();
        Dog alphaDog=new Dog(name);
        
        
        
        do{
            System.out.print("Mitä koira sanoo: ");
            message=dogReader.readLine();
            alphaDog.speak(message);
        } while (message.trim().isEmpty());
    }
    
}
