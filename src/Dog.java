
import java.util.Scanner;

    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Dog {
    private String myName;

    public Dog (String name){
        if (name.trim().isEmpty())
            myName="Doge";
        else
            myName=name;
        System.out.println("Hei, nimeni on " + myName);
    }
    
    public Dog () {
        myName="Doge";
    }
    
    public void speak(String words){
        //boolean smart = false;
    
        if (words.trim().isEmpty())
            words="Much wow!";
        //else {
            Scanner dogBrain= new Scanner(words);
            while(dogBrain.hasNext()){
                if(dogBrain.hasNextBoolean()){
                    System.out.println("Such boolean: "+dogBrain.nextBoolean());
                //    smart = true;
                } else if(dogBrain.hasNextInt()){
                    System.out.println("Such integer: "+dogBrain.nextInt());
                //    smart = true;
                } else {
                    System.out.println(dogBrain.next());
                }}
    //}
        //if(!smart)
        //   System.out.println(myName+": "+words);
    }
    
    public void speak(){
        System.out.println("Much wow!");
    }
}
